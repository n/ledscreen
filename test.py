#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
from vedirect import Vedirect
from ledscreen import Ledscreen
from datetime import datetime, timedelta

import pprint
pp = pprint.PrettyPrinter(indent=4)
#pp.pprint(stuff)
'''
FW 119 -- Firmware version of controller, v1.19
SER# HQXXXXXXXXX -- Serial number
V 13790 -- Battery voltage, mV
I -10 -- Battery current, mA
VPV 15950 -- Panel voltage, mV
PPV 0 -- Panel power, W
CS 5 -- Charge state, 0 to 9
ERR 0 -- Error code, 0 to 119
LOAD ON -- Load output state, ON/OFF
IL 0 -- Load current, mA
H19 0 -- Yield total, kWh
H20 0 -- Yield today, kWh
H21 397 -- Maximum power today, W
H22 0 -- Yield yesterday, kWh
H23 0 -- Maximum power yesterday, W
HSDS 0 -- Day sequence number, 0 to 365

    'FW': dict ( 'system/firmware',
    'MPPT': 'now/mppt-state',
    'PPV': 'now/pv-power',

'''
victron_key_map = {
    #'LOAD': { "nicename": 'Load output state, ON/OFF', "type": str},
    #'VPV':  { "nicename": 'Panel voltage, mV', "type": float, 'div': 1000.0},
    'CS': { 'd': True, "sn": 'charge', "nicename": 'Charge state, 0 to 9', "type": str},
    'ERR': {'d': False, "sn": "error", "nicename": 'ERRor code, 0 to 119', "type": str},
    'H19': {"sn": "KWH   total", "nicename": 'Yield total, kWh', "type": float, 'div': 100},
    'H20': {'d': True, "sn": "KWH    today", "nicename": 'Yield today, kWh', "type": float, 'div': 100},
    'H21': { 'd': True, "sn": "MAX    WATTS", "nicename": 'Maximum power today, W', "type": float, 'div': 100},
    'H22': { "sn": "KWhYest", "nicename": 'Yied yesterday, kWh', "type": float, 'div': 100},
    'H23': { "sn": "MAXyest", "nicename": 'Maximum power yesterday, W', "type": float},
    'HSDS': { "sn": "Day", "nicename": 'Day sequence number, 0 to 365', "type": int},
    'I': { 'd': True, "sn": "BAT    AMPS", "nicename": 'Battery Current, mA', "type": float, "div": 1000.0},
    'PID': {"sn": "PID", "nicename": 'Product id'},
    'PPV': { 'd': True, "sn": "PV     POWER", "nicename": 'PPV??'},
    'SER#': { "sn": "serial", "nicename": 'Serial number'},
    'V': { 'd': True, "sn": "BAT    volt", "nicename": 'Battery voltage, mV', "type": float, "div": 1000.0},
    'VPV': { 'd': True, "sn": "PV     VOLT", "nicename": 'VPV??', "type": float, "div": 1000.0},
    'FWE': { 'd': False, "sn": "FWE", "nicename": 'FWE??'},
    "\r\nPID": { "sn": "PID", "nicename": "pid"},
    "E\nPID": { "sn": "PID", "nicename": "pid"},
    'AC_OUT_I': { 'd': True, "sn": "AC     AMPS", "nicename": 'AC out current', "type": float},
    'AC_OUT_V': { 'd': False, "sn": "AC    VOLT", "nicename": 'AC out voltage', "type": float, "div": 1000.0},
 
}

class Connector:
    
    victrondata = dict()
    keyiter = iter(victron_key_map)
    currentitem = 'CS'
    lastdisplay = datetime.strptime('Jun 1 2005  1:33PM', '%b %d %Y %I:%M%p')

    def __init__(self, serial, serial_screen):
        self.screen = Ledscreen(port=serial_screen, timeout=1)
        self.screen.write("Hello   Hello  Hello")
 
        self.ve = Vedirect(port=serial, timeout=1)
        self.ve.read_data_callback(self.on_victron_data_callback)
        
        #self.lastdisplay = now()

    def update_display(self):
        now = datetime.now()
        #print('.')
        #self.screen.write(str(self.currentitem) + ':' + str(self.currentitem) )     
        current_time = now.strftime("%H:%M:%S")
        last_time = self.lastdisplay.strftime("%H:%M:%S")

        if self.currentitem in self.victrondata:
            #print('updateing display for ' + self.currentitem)
            if 'sn' in victron_key_map[self.currentitem]:
                sn = victron_key_map[self.currentitem]['sn']
            else: 
                sn = "NN     "
            
            val = str(self.victrondata[self.currentitem]['value'])
            self.screen.write(f"{sn:13.13}"+ ' ' + f"{val:6.6}")
        else:
            print('data not available for ' + str(self.currentitem))

        #print(timedelta(seconds=10))
        #print(current_time + '  ' + last_time) 
        if now - self.lastdisplay > timedelta(seconds=5):
            self.lastdisplay = now
            current_time = now.strftime("%H:%M:%S")
            #print("Current Time =", current_time)
            #self.screen.write(current_time)

            try:
                self.currentitem = next(self.keyiter)
            except StopIteration:
                self.keyiter = iter(victron_key_map)


            while not(self.currentitem in victron_key_map and 'd' in victron_key_map[self.currentitem] and victron_key_map[self.currentitem]['d']):
                print('') 
                print(self.currentitem)
                print(self.currentitem in self.victrondata and self.currentitem in victron_key_map) 
                print('d' in victron_key_map[self.currentitem] and victron_key_map[self.currentitem]['d'])


                print('bad item: ' + self.currentitem)
                try:
                    self.currentitem = next(self.keyiter)
                except StopIteration:
                    self.keyiter = iter(victron_key_map)
      
            print(current_time + 'next item' + str(self.currentitem))
            pp.pprint(self.currentitem)
    
    # Local variable to store previous frame
    # previous_victron_frame = {}

    def on_victron_data_callback(self, data):
       # pp.pprint(data)
        # global previous_victron_frame
        #try:
        for key, value in data.items():
            # NB: To filter values, do something like this:
            # if previous_victron_frame.get(key, None) == value:
            #     continue
            # else:
            #     previous_victron_frame[key] = value
            try:
                if key == 'CS':
                    value = Vedirect.VICTRON_CS[value]
                elif key == 'MPPT':
                    value = Vedirect.VICTRON_CS[value]
                elif key == 'ERR':
                    value = Vedirect.VICTRON_ERROR[value]
            except KeyError:
                value = "unk: " + value

                
            #elif key == 'V':
            #    value = str (int ( value ) / 1000.0) 

            if key in victron_key_map:
                if 'type' in victron_key_map[key]:
                    #print("casting: " + key + ' ' +value + "from" + str(type(value)) + "to " + str(type(victron_key_map[key]['type'](value)) ))
                    value = victron_key_map[key]['type'](value)
                    if 'div' in victron_key_map[key]:
                        value = value / victron_key_map[key]['div']

                self.victrondata[key] = { "value": value, "nicename": victron_key_map[key]["nicename"] }
           
            #print(name + "\t\t\t\t\t: " + str(value) )

            #print(self.victrondata.keys())
            #now = datetime.now()
            self.update_display()
            #current_time = now.strftime("%H:%M:%S")
            #print("Current Time =", current_time)
            #self.screen.write(current_time)


            #self.screen.write("Voltage: " + value)
            #print(key + ": " + value)
        #except ValueError:
        #   print('something wrong')



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse Ve.Direct serial data onto screen')
    parser.add_argument('--serial-victron', help='victron serial port', required=False, default='/dev/serial/by-id/usb-VictronEnergy_BV_VE_Direct_cable_VE6SIUSZ-if00-port0',)
    parser.add_argument('--serial-screen', help='screen serial port', required=False, default='/dev/serial/by-id/usb-1a86_USB_Serial-if00-port0')
    args = parser.parse_args()

    connector = Connector(args.serial_victron, args.serial_screen)
#    connector = Connector()
