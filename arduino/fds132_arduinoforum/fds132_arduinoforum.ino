//  code found on arduinoforum.nl
// TODO:
// - wire up the even text lines and make them work


// Timer 1 Info :  http://playground.arduino.cc/code/timer1
// Using PROGMEM : http://arduino.cc/en/Reference/PROGMEM


// The matrix board is controlled by timer0 overflow interrupt. This gives a steady image and the display can be handled like a static type.
// The main code does not have to control the board, just set or clear bits in the data array to put some led's on or off.
// Logically it's organized in colums, 270 in total. This sofware defines column 0 as the upper left column and 271 is the right bottom column.
// This makes it more easy to arrange the text on the board. There is no checking if the text fits on the display.
//
// Column layout :    Line 0 : 0 to 89
//                    Line 1 : 90 to 179
//                    Line 1 : 180 to 271
//
//
//

const byte numChars = 26;
char receivedChars[numChars];   // an array to store the received data

boolean newData = false;
 
//#include <avr/pgmspace.h>             // Include pgmspace so data tables can be stored in program memory, see PROGMEM info
#include <SPI.h>       // Include SPI comunications, used for high speed clocking to display shiftregisters
#include "TimerOne.h"  // Include Timer1 library, used for timer overflow interrupt
//#include <SoftwareSerial.h>
// 28 pins
// >> 0-7 port D
// 0 TXD
// 1 RXD
// 2
#define victron_rx 2
// 3
#define victron_tx 3
// 4
const int testpin = 4;

// 5
const int row_a = 5;
// 6
const int row_b = 6;
// 7
const int row_c = 7;
// >> 8-13 port B
// 8
// 9
const int resredPin = 9;
// 10
const int strobePin = 10;  // Define I/O pins connected to the display board
// 11 mosi
const int dataPin = 11;
// 12 miso
// 13 sck
const int clockPin = 13;

#define softser = 0

#include <avr/io.h>
#include <avr/interrupt.h>

#define BAUD_RATE 19200

bool testje = false;

//SoftwareSerial vicSer = SoftwareSerial(victron_rx, victron_tx, false);



int RowCount = 0;  // Counter for the current row, do not change this value in the main code, it's handeld in the interrupt routine

#include "font75.h";  // Standard 5x7 font ASCII Table

byte ColumnBytes[8][34];  // This is the data array that is used as buffer. The display data is in this buffer and the timeroverflow routine scans this
                          // buffer and shows it on the display. So read and write to this buffer to change display data.
                          // No strings are stored here, its the raw bit data that has to be shifted into the display.








void setup() {


  /////
  pinMode(strobePin, OUTPUT);  // Define IO
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(row_c, OUTPUT);
  pinMode(row_b, OUTPUT);
  pinMode(row_a, OUTPUT);
  pinMode(resredPin, OUTPUT);
  pinMode(testpin, OUTPUT);

  digitalWrite(testpin, LOW);
  // TODO: Odd even blocks

  digitalWrite(resredPin, LOW);
  digitalWrite(strobePin, LOW);
  Serial.begin(9600);                      // Include serial routine, used for debugging and testing
  SPI.begin();                             // Start SPI comunications
  SPI.setBitOrder(LSBFIRST);               // Initialize SPI comunications
  Timer1.initialize(1000);                 // initialize timer1, overflow timer1 handles the display driver, period (in microseconds)
  //Timer1.initialize(2000);                 // initialize timer1, overflow timer1 handles the display driver, period (in microseconds)
  Timer1.attachInterrupt(DisplayControl);  // attaches timer1 overflow interrupt routine
/*
  // VE.Direct softserial
  pinMode(victron_rx, INPUT);
  pinMode(victron_tx, OUTPUT);
  //vicSer.begin(19200);

    // Calculate the timer value for the desired baud rate
    //unsigned int timer_val = (F_CPU / (BAUD_RATE * 16UL)) - 1;
    //unsigned int timer_val = (16000000 / (192000 * 16UL)) - 1;

    // Set the Timer1 registers with the calculated value
    //TCNT1H = (timer_val >> 8);
    //TCNT1L = (timer_val & 0xFF);
    
    // Set Timer1 prescaler to 8
    TCCR1B |= (1 << CS11) | (1 << CS);
    
    // Enable Timer1 Overflow Interrupt
    TIMSK1 |= (1 << TOIE1);
    
    // Enable global interrupts
    sei();
  cli();
  sei();
*/
  ClearDisplay();

  PlaceText("hee hallo", 0);

/*
  if (vicSer.isListening()) {
    Serial.println("portOne is listening!");
  }
  */
  for (int i = 3; i > 0; i--) { // pause a little to give you time to open the Arduino monitor
    Serial.print(i); Serial.print(' '); delay(500);
  }
  Serial.println();

}

/*
// Timer1 Overflow Interrupt Service Routine
ISR(TIMER1_OVF_vect) {

  if (digitalRead(testpin) == LOW) {
    digitalWrite(testpin, HIGH);
  } else {
    digitalWrite(testpin, LOW);
  }
}
*/
int loopcount = 0;


void recvWithEndMarker() {
    static byte ndx = 0;
    char endMarker = '\n';
    char rc;
    
    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (rc != endMarker) {
            receivedChars[ndx] = rc;
            ndx++;
            if (ndx >= numChars) {
                ndx = numChars - 1;
            }
        }
        else {
            receivedChars[ndx] = '\0'; // terminate the string
            ndx = 0;
            newData = true;
        }
    }
}

void showNewData() {
    if (newData == true) {
        PlaceText(receivedChars, 0);
        Serial.print("New text: ");
        Serial.println(receivedChars);
        newData = false;
    }
}
void loop() {
    recvWithEndMarker();
    showNewData();
  //delay(1000);
  loopcount = loopcount + 1;

/*
  if (vicSer.overflow()) {
    Serial.println("vedirect receive buffer overflow!");
  }
*/

/*
  if (loopcount & (1 << 2)) {
    ClearDisplay();
    char buf[32];
    sprintf(buf, "%u %u test test test", loopcount, 123);
    PlaceText(buf, 0);
  }
  */

  //Serial.println("END victron buffer");

  /*


  PlaceText("1", 0);
  delay(500);
  PlaceText("2", 5);
  delay(500);
  PlaceText("3", 10);
  delay(500);
  PlaceText("4", 15);

  delay(500);vedirect baudrate
  delay(500);
  PlaceText("C", 55);
  delay(500);
  PlaceText("D", 60);
  delay(500);

  PlaceText("E", 65);
  delay(500);

  PlaceText("F", 70);
  delay(500);

  PlaceText("G", 75);
  delay(500);

  PlaceText("H", 80);
  delay(500);

  PlaceText("I", 85);
  delay(500);


  PlaceText("J", 90);
  delay(500);

  PlaceText("K", 95);
  delay(500);

  PlaceText("L", 100);
  delay(500);

  PlaceText("M", 105);
  delay(500);

  PlaceText("N", 110);
  delay(500);

  PlaceText("O", 115);
  delay(500);

  ClearDisplay();
  PlaceText("Dit iss een testje!!!!!", 0);
  delay(15000);
  */
}


// ==
//
/*
void readVeDirect() {

#define lines 19
#define linelength 60
char line[linelength];

  int charcount = 0;

  if (vicSer.available() > linelength){

  
  while (vicSer.available() > 0 && charcount <= linelength) {
    line[charcount] = vicSer.read();
    Serial.print(line[charcount]);
    if (line[charcount] == '\t' || charcount > linelength) {
      line[charcount] = '\0';
    }

    charcount++;
  }
  Serial.println(line);
  //Serial.println(line);
  Serial.println(charcount);
  }
}
*/

// =========================================================================================
// Timer1 overflow intterupt routine,
// Does the Display shifting and muliplexing
// =========================================================================================



void DisplayControl() {

  //Serial.println("START victron buffer");
  //readVeDirect();


  digitalWrite(strobePin, LOW);  // StrobePin LOW, so led's do not change during clocking
  //digitalWrite (resredPin, LOW);                       // Display off to prevent ghosting

  for (int q = 0; q < 34; q++) {  // Shift the bytes in for the current Row
//digitalWrite(strobePin, LOW); 
    SPI.transfer(ColumnBytes[RowCount][q]);
  //digitalWrite(strobePin, HIGH); 
  }

  digitalWrite(row_a, RowCount & 1);  // Switch the current column on, will be on until next interrupt
  digitalWrite(row_b, RowCount & 2);
  digitalWrite(row_c, RowCount & 4);

  digitalWrite(strobePin, HIGH);  // Strobe the shiftregisters so data is presented to the LED's
  //digitalWrite (resredPin, HIGH);                      // Display back on

  RowCount++;                       // Increment Row
  if (RowCount == 7) RowCount = 0;  // Row is 0 to 6
}

// =========================================================================================
// Place text on display, uses the standard 5x7 font
// Call this routine with the text to display and the starting column to place the text
// Example : PlaceText("FDS132",27); Places the text "FDS132" starting on column 27
// =========================================================================================

void PlaceText(String text, int ColPos) {
  byte displayByte;
  char CurChar;
  int bitpos;
  int bytepos;
  int pixelpos;
  int charCount = text.length();
  int store = ColPos;
  //ClearDisplay();
  for (int i = 0; i < charCount; i++) {  // Loop for all the characters in the string
    CurChar = text.charAt(i);            // Read ascii value of current character
    CurChar = CurChar - 32;              // Minus 32 to get the right pointer to the ascii table, the first 32 ascii code are not in the table

    for (int y = 0; y < 7; y++) {                                        // y loop is used to handle the 7 rows
      for (int x = 0; x < 5; x++) {                                      // x loop is the pointer to the indiviual bits in the byte from the table
        displayByte = (pgm_read_word_near(Font75 + (CurChar * 7) + y));  // Read byte from table
        pixelpos = abs(ColPos - 271);                                    // Calculate start position to place the data on display
        bytepos = ((pixelpos) / 8);
        bitpos = (pixelpos) - (bytepos * 8);
        boolean bitStatus = bitRead(displayByte, x);                    // Probe the bits
        if (bitStatus == 0) bitClear(ColumnBytes[y][bytepos], bitpos);  // And set or clear the corrosponding bits in de display buffer
        if (bitStatus == 1) bitSet(ColumnBytes[y][bytepos], bitpos);
        ColPos++;
      }
      ColPos = store;  // Reset the column pointer so the next byte is shown on the same position
    }
    ColPos = ColPos + 6;  // 6 is used here to give one column spacing between characters (one character is 5 bits wide)
    store = ColPos;       // For more space between the characters increase this number
  }
}

// =========================================================================================
// Clear display
// All bytes in buffer are set to zero
// =========================================================================================

void ClearDisplay() {
  for (int y = 0; y < 8; y++) {
    for (int x = 0; x < 34; x++) {
      ColumnBytes[y][x] = 0;
    }
  }
}
