import serial
import time
"""
ledscreen
"""
class Ledscreen:
    def __init__(self, port='/dev/ttyUSB1', timeout=5):
        """
        Initialise serial component of the Victron parser. Default value is the standard serial port on Raspberry pi
        :param port:
        :param timeout:
        """
        self.ser = serial.Serial(port=port, baudrate=9600, timeout=.1)


        print('from screen: ' + str(self.ser.readline()))
        x = 'testing123' + '\n' 
        self.ser.write(bytes(x, 'utf-8'))
        time.sleep(0.05)
        print('from screen: ' + str(self.ser.readline()))
   
    def write(self, string):
        x = f"{string:<24}"  + '\n' 
        self.ser.write(bytes(x, 'utf-8'))
        ret = str(self.ser.readline())
 

