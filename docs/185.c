 // FDS185 using Timer1.  
 // Based on sketch from Rolo for FDS-132, see http://arduinoforum.nl/viewtopic.php?f=6&t=336&start=50#p3026  
 // Rudi Imbrechts - 06/12/2013  
 // Places text on any line at any position.  
 // Also now can place a one digit content of a variable.  
 // So, here is a digital clock, including a relative humidity & temperature indicator.  
 //  
 // Timer 1 Info : http://playground.arduino.cc/code/timer1  
 // Using PROGMEM : http://arduino.cc/en/Reference/PROGMEM  
 //  
 // The matrix board is controlled by timer0 overflow interrupt. This gives a steady image and the display can be handled like a static type.  
 // The main code does not have to control the board, just set or clear bits in the data array to put some led's on or off.  
 // Logically it's organized in colums, 240 in total. This sofware defines column 0 as the upper left column and 239 is the right bottom column.  
 // This makes it more easy to arrange the text on the board. There is no checking if the text fits on the display.  
 //  
 // A ledmatrixboard FDS185 consists of 60 ledmatrices, 6 per line, 10 lines in total.  
 // Each ledmatrix has 64 leds (8 columns, 8 rows) so each line has 48 columns.   
 // Hardwarewise there are 16 rows, selectable as rows 1-8 and rows 9-16. Odd lines are controlled by rows 1-8, even lines by rows 9-16.  
 //  
 //   FDS-185  
 //   ########   Line 1 : columns 1 to 48, rows 1-8 -> content in 'oddlines' buffer  
 //   ########   Line 2 : columns 1 to 48, rows 9-16 -> content in 'evenlines' buffer  
 //   ########   Line 3 : columns 49 to 96, rows 1-8 -> content in 'oddlines' buffer  
 //   ########   Line 4 : columns 49 to 96, rows 9-16 -> content in 'evenlines' buffer  
 //   ########   Line 5 : columns 97 to 144, rows 1-8 -> content in 'oddlines' buffer  
 //   ########   Line 6 : columns 97 to 144, rows 9-16 -> content in 'evenlines' buffer  
 //   ########   Line 7 : columns 145 to 192, rows 1-8 -> content in 'oddlines' buffer  
 //   ########   Line 8 : columns 145 to 192, rows 9-16 -> content in 'evenlines' buffer  
 //   ########   Line 9 : columns 193 to 240, rows 1-8 -> content in 'oddlines' buffer  
 //   ########   Line 10 : columns 193 to 240, rows 9-16 -> content in 'evenlines' buffer  
 //  
 #include <avr/pgmspace.h>           // Include pgmspace so data tables can be stored in program memory, see PROGMEM info  
 #include <SPI.h>               // Include SPI comunications, used for high speed clocking to display shiftregisters  
 #include "TimerOne.h"             // Include Timer1 library, used for timer overflow interrupt.  
 #include "RTClib.h"              // Include library for RTC.  
 #include <Wire.h>               // Library needed for RTC.  
 #include "DHT.h"               // Include library for the DHT sensor  
 const int DHTPIN = 2;   // what pin we're connected to  
 #define DHTTYPE DHT11  // DHT 11   
 RTC_DS1307 RTC;                //   
 const int rtcPlusPin = 17;          // RTC module +5V connection.  
 const int rtcMinPin = 16;           // RTC module GND connection.  
 DHT dht(DHTPIN, DHTTYPE);  
 const int resred = 9;             // Define I/O pins connected to the display board.  
 const int strobe = 10;                 
 const int data = 11;                  
 const int clock = 13;                  
 const int row_a = 5;                     
 const int row_b = 6;                    
 const int row_c = 7;   
 const int oddLine = 4;            // oddLine selects rows 1 to 8 (enables chip U3).  
 const int evenLine = 8;            // evenLine selects rows 9 to 16 (enables chip U6).  
 int rowCount = 0;               // Counter for the current row, do not change this value in the main code, it's handled in the interrupt routine   
 int toggle = 0;                // toggle buffers. If zero then 'oddlines' buffer is selected, if 1 then 'evenLines' buffer.  
 #include "font75.h";             // Standard 5x7 font ASCII Table  
 byte oddLinesBuffer[8][30];          // This is the data array that is used as buffer for the odd lines. The display data is in this buffer and the timer overflow  
                        // routine scans this buffer and shows it on the display. So read and write to this buffer to change display data.  
                        // No strings are stored here, its the raw bit data that has to be shifted into the display.    
 byte evenLinesBuffer[8][30];         // This is the data array that is used as buffer for the even lines.   
 int hourUnits = 0;              // Integers for the clock  
 int hourTens = 0;  
 int minutesUnits = 0;  
 int minutesTens = 0;  
 int secondsUnits = 0;  
 int secondsTens = 0;  
 int tempUnits = 0;  
 int tempTens = 0;  
 int humUnits = 0;  
 int humTens = 0;  
 int savedSecondsTens = secondsTens;  
 // =========================================================================================  
 // Do the setup  
 // =========================================================================================  
  void setup() {    
  pinMode (strobe, OUTPUT);          // Define IO       
  pinMode (clock, OUTPUT);   
  pinMode (data, OUTPUT);   
  pinMode (row_c, OUTPUT);   
  pinMode (row_b, OUTPUT);   
  pinMode (row_a, OUTPUT);   
  pinMode (resred, OUTPUT);   
  pinMode (oddLine, OUTPUT);  
  pinMode (evenLine, OUTPUT);  
  pinMode (rtcMinPin, OUTPUT);  
  pinMode (rtcPlusPin, OUTPUT);   
  digitalWrite(rtcMinPin, LOW);        // GND for RTC module  
  digitalWrite(rtcPlusPin, HIGH);       // +5V for RTC module  
  digitalWrite (oddLine, HIGH);        // disable rows 1-8  
  digitalWrite (evenLine, HIGH);       // disable rows 9-16  
  digitalWrite (resred, HIGH);        // disable output  
  digitalWrite (strobe, HIGH);        // disable latch  
  //Serial.begin(9600);            // Include serial routine, used for debugging and testing  
  SPI.begin();                // Start SPI communications  
  SPI.setBitOrder(LSBFIRST);         // Initialize SPI communications  
  Timer1.initialize(600);          // initialize timer1, overflow timer1 handles the display driver, period (in microseconds)  
  Timer1.attachInterrupt(displayControl);   // attaches timer1 overflow interrupt routine  
  Wire.begin();  
  RTC.begin();                // initialize the clock  
  //RTC.adjust(DateTime(__DATE__, __TIME__)); // load time from computer to RTC (only oince if RTC time not set).    
  dht.begin();                // initialize DHT  
  ClearDisplay();               // Initial text that will be displayed can be defined here   
  placeTextOddLinesBuffer("The time",48*0);  // place fixed text  
  placeTextEvenLinesBuffer("is now :",48*0);  
  placeTextEvenLinesBuffer("  hour",48*1);  
  placeTextOddLinesBuffer("  min",48*2);  
  placeTextEvenLinesBuffer("  sec",48*2);  
  placeTextOddLinesBuffer("temp  C",48*4);  
  placeTextEvenLinesBuffer("hum  %",48*4);  
  }   
 // =========================================================================================  
 // loop routine, mandatory  
 // Take notice when using time crititical code the interrupt is running to control   
 // the display.  
 // =========================================================================================  
  void loop()   
  {   
  getTime();   
  placeCharEvenLinesBuffer(hourTens, 48*1+3);  
  placeCharEvenLinesBuffer(hourUnits, 48*1+9);  
  placeCharOddLinesBuffer(minutesTens, 48*2+3);  
  placeCharOddLinesBuffer(minutesUnits, 48*2+9);  
  placeCharEvenLinesBuffer(secondsTens, 48*2+3);  
  placeCharEvenLinesBuffer(secondsUnits, 48*2+9);  
  if bitRead(secondsUnits,0) {                       // a simple animation 
   placeTextEvenLinesBuffer(" /",48*3+20);  
   } else {  
   placeTextEvenLinesBuffer("\\ ",48*3+20);  
   }  
  if (savedSecondsTens != secondsTens) {             // check if 10 seconds are passed  
  getHT();                                           // if so then get and show temp and RH values
  placeCharOddLinesBuffer(tempTens, 48*4+28);  
  placeCharOddLinesBuffer(tempUnits, 48*4+35);  
  placeCharEvenLinesBuffer(humTens, 48*4+28);  
  placeCharEvenLinesBuffer(humUnits, 48*4+35);  
  savedSecondsTens = secondsTens;  
  }  
  }   
 // =========================================================================================  
 // Timer1 overflow intterupt routine,   
 // Does the display shifting and muliplexing  
 // =========================================================================================  
  void displayControl()   
  {   
    toggle = !toggle;            // toggle between both buffers  
    digitalWrite (oddLine, !toggle);     // Activate or odd or even lines.  
    digitalWrite (evenLine, toggle);  
    digitalWrite(resred, LOW);        // enable the display.  
    digitalWrite(strobe, LOW);        // update shiftregisters output.  
    for (int q = 0; q<30; q++)  
      {   
      if (toggle == 0)           // Shift the bytes in for an odd numbered line,      
       {  
       SPI.transfer(oddLinesBuffer[rowCount][q]);    
       }  
      else                 // Shift the bytes in for an even numbered line.  
       {              
       SPI.transfer(evenLinesBuffer[rowCount][q]);  
       }    
      }  
    digitalWrite (resred, HIGH);       // display off to prevent ghosting   
    digitalWrite (strobe, HIGH);       // strobe HIGH, so led's do not change during clocking  
    digitalWrite (row_a, rowCount & 1);   // Switch the current column on, will be on until next interrupt  
    digitalWrite (row_b, rowCount & 2);          
    digitalWrite (row_c, rowCount & 4);      
    rowCount++;               // increment row   
    if (rowCount == 7) rowCount = 0 ;    // row is 0 to 6  
  }   
 // =========================================================================================  
 // Place text on display, uses the standard 5x7 font  
 // Call this routine with the text to display and the starting column to place the text  
 // on the lower rows (1-8).  
 // Example : placeTextOddLinesBuffer("FDS185",27); Places the text "FDS185" starting on column 27  
 //      of the first line, which is the first odd line.  
 // =========================================================================================  
 void placeTextOddLinesBuffer(String text, int colPos) {  
   byte displayByte;   
   char curChar;  
   int bitPos;  
   int bytePos;  
   int pixelPos;  
   int charCount = text.length();  
   int store = colPos;  
   for (int i = 0; i < charCount; i++) {                   // Loop for all the characters in the string  
   curChar = text.charAt(i);                        // Read ascii value of current character  
   curChar = curChar - 32;                         // Minus 32 to get the right pointer to the ascii table, the first 32 ascii code are not in the table    
    for (int y = 0; y<7; y++) {                       // y loop is used to handle the 7 rows  
     for (int x = 0; x<5; x++) {                      // x loop is the pointer to the indiviual bits in the byte from the table   
      displayByte = (pgm_read_word_near(Font75 + (curChar*7)+y));     // Read byte from table   
      pixelPos = abs(colPos - 239);                    // Calculate start position to place the data on display  
      bytePos = ((pixelPos)/8);                        
      bitPos = (pixelPos) - (bytePos*8);  
      boolean bitStatus = bitRead(displayByte, x);            // Probe the bits    
      if(bitStatus == 0) bitClear(oddLinesBuffer[y][bytePos], bitPos);  // And set or clear the corresponding bits in de display buffer    
      if(bitStatus == 1) bitSet(oddLinesBuffer[y][bytePos], bitPos);      
      colPos++;                                
    }  
     colPos = store;                            // Reset the column pointer so the next byte is shown on the same position   
    }  
   colPos = colPos + 6;                           // 6 is used here to give one column spacing between characters (one character is 5 bits wide)  
   store = colPos;                             // For more space between the characters increase this number   
  }  
 }     
 // =========================================================================================  
 // Same routine as placeTextOddLinesBuffer() but for the higher rows (9-16).  
 // Example : placeTextEvenLinesBuffer("FDS185",27); Places the text "FDS185" starting on column 27  
 //      of the second line which is the first even line.  
 // =========================================================================================  
 void placeTextEvenLinesBuffer(String text, int colPos) {             
   byte displayByte;   
   char curChar;  
   int bitPos;  
   int bytePos;  
   int pixelPos;  
   int charCount = text.length();  
   int store = colPos;  
   for (int i = 0; i < charCount; i++) {                   
   curChar = text.charAt(i);   
   curChar = curChar - 32;   
    for (int y = 0; y<7; y++) {    
     for (int x = 0; x<5; x++) {   
      displayByte = (pgm_read_word_near(Font75 + (curChar*7)+y));  
      pixelPos = abs(colPos - 239);   
      bytePos = ((pixelPos)/8);                        
      bitPos = (pixelPos) - (bytePos*8);  
      boolean bitStatus = bitRead(displayByte, x);    
      if(bitStatus == 0) bitClear(evenLinesBuffer[y][bytePos], bitPos);    
      if(bitStatus == 1) bitSet(evenLinesBuffer[y][bytePos], bitPos);      
      colPos++;                                
    }  
     colPos = store;   
    }  
   colPos = colPos + 6;   
   store = colPos;   
  }  
 }  
 // =========================================================================================  
 // Clear display  
 // All bytes in buffer are set to zero  
 // =========================================================================================  
 void ClearDisplay() {  
   for (int y = 0; y<8; y++) {     
     for (int x = 0; x<30; x++) {     
      oddLinesBuffer[y][x] = 0;  
      evenLinesBuffer[y][x] = 0;  
     }  
   }   
 }  
 // =========================================================================================  
 // Place a character on display, uses the standard 5x7 font  
 // Call this routine with an integer value to display and the starting column to place the text  
 // on the lower rows (1-8).  
 // Example : placeCharOddLinesBuffer(character,27); Places the value of integer character, starting on column 27  
 //      of the first line, which is the first odd line.  
 // =========================================================================================  
 void placeCharOddLinesBuffer(int character, int colPos) {  
   byte displayByte;   
   char curChar;  
   int bitPos;  
   int bytePos;  
   int pixelPos;  
   int store = colPos;  
   curChar = character + 16;                        // Read ascii value of character and add 16 in order to match character in font75.h     
    for (int y = 0; y<7; y++) {                       // y loop is used to handle the 7 rows  
     for (int x = 0; x<5; x++) {                      // x loop is the pointer to the indiviual bits in the byte from the table   
      displayByte = (pgm_read_word_near(Font75 + (curChar*7)+y));     // Read byte from table   
      pixelPos = abs(colPos - 239);                    // Calculate start position to place the data on display  
      bytePos = ((pixelPos)/8);                        
      bitPos = (pixelPos) - (bytePos*8);  
      boolean bitStatus = bitRead(displayByte, x);            // Probe the bits    
      if(bitStatus == 0) bitClear(oddLinesBuffer[y][bytePos], bitPos);  // And set or clear the corresponding bits in de display buffer    
      if(bitStatus == 1) bitSet(oddLinesBuffer[y][bytePos], bitPos);      
      colPos++;                                
    }  
     colPos = store;                            // Reset the column pointer so the next byte is shown on the same position   
    }  
   colPos = colPos + 6;                           // 6 is used here to give one column spacing between characters (one character is 5 bits wide)  
   store = colPos;                             // For more space between the characters increase this number   
  }  
 // =========================================================================================  
 // Place a character on display, uses the standard 5x7 font  
 // Call this routine with an integer value to display and the starting column to place the text  
 // on the lower rows (1-8).  
 // Example : placeCharOddLinesBuffer(character,27); Places the value of integer character, starting on column 27  
 //      of the first line, which is the first odd line.  
 // =========================================================================================  
 void placeCharEvenLinesBuffer(int character, int colPos) {  
   byte displayByte;   
   char curChar;  
   int bitPos;  
   int bytePos;  
   int pixelPos;  
   int store = colPos;  
   curChar = character + 16;                        // Read ascii value of character and add 16 in order to match character in font75.h     
    for (int y = 0; y<7; y++) {                       // y loop is used to handle the 7 rows  
     for (int x = 0; x<5; x++) {                      // x loop is the pointer to the indiviual bits in the byte from the table   
      displayByte = (pgm_read_word_near(Font75 + (curChar*7)+y));     // Read byte from table   
      pixelPos = abs(colPos - 239);                    // Calculate start position to place the data on display  
      bytePos = ((pixelPos)/8);                        
      bitPos = (pixelPos) - (bytePos*8);  
      boolean bitStatus = bitRead(displayByte, x);            // Probe the bits    
      if(bitStatus == 0) bitClear(evenLinesBuffer[y][bytePos], bitPos);  // And set or clear the corresponding bits in de display buffer    
      if(bitStatus == 1) bitSet(evenLinesBuffer[y][bytePos], bitPos);      
      colPos++;                                
    }  
     colPos = store;                            // Reset the column pointer so the next byte is shown on the same position   
    }  
   colPos = colPos + 6;                           // 6 is used here to give one column spacing between characters (one character is 5 bits wide)  
   store = colPos;                             // For more space between the characters increase this number   
  }  
 // =========================================================================================  
 // get the time from the RealTimeClock  
 // =========================================================================================  
 void getTime()        
 {  
 DateTime now = RTC.now();                          // read RTC clock.  
 int seconds = now.second();                         // get seconds,  
 secondsUnits = seconds%10;                          // split them into units,  
 secondsTens = seconds/10;                          // and tens.  
 int minutes = now.minute();                         // get minutes  
 minutesUnits = minutes%10;  
 minutesTens = minutes/10;  
 int hours = now.hour();                           // get hours  
 hourUnits = hours%10;  
 hourTens = hours/10;  
 }  
 // =========================================================================================  
 // Get data from the digital humidity and temperature sensor DHT11  
 // =========================================================================================  
 void getHT()  
 {  
 int hum = dht.readHumidity();  
 int temp = dht.readTemperature();  
 humUnits = hum%10;  
 humTens = hum/10;  
 tempUnits = temp%10;  
 tempTens = temp/10;  
 }  
