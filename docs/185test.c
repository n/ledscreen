 /*  
 Aansturen FDS185 LED matrix bord  
 Een FDS185 bestaat uit 6 * 10 dotmatrix displays met elk 8*8 leds wat een totaal oplevert van 60*64 = 3840 leds!  
 Deze leds worden ingedeeld/aangestuurd door 16 rijen en 240 kolommen (16 maal 240 is ook 3840 :-))  
 Iedere kolom hangt aan een uitgang van een shiftregister, iedere rij wordt door een FET aan- of uitgezet.  
 Eenvoudige code die alle leds doet oplichten m.b.v. shiftout code.  
 */  
                                                 // Pinnen voor het aansturen van het FDS185 bord.  
 const int strobePin = 10;                       // pin voor strobe signaal, ook wel latch genaamd.  
 const int clockPin = 13;                        // pin met clock signaal.  
 const int dataPin = 11;                         // pin voor het serieel sturen van data.  
 const int resredPin = 9;                        // resred, ook wel OutputEnable of OE genoemd.  
 const int row_a = 5;                            // ieder ledmatrix heeft 8 rijen. Hardwarematig   
 const int row_b = 6;                            // gebruiken we een 3-naar-8 decoder van het  
 const int row_c = 7;                            // type 74HC238 (U3 en U6 op het schema).  
 const int lowRowPin = 4;                        // lowRowPin stuurt rijen 0 t/m 7 (selecteert chip U3).  
 const int highRowPin = 8;                       // highRowPin stuurt rijen 8 t/m 15 (selecteert chip U6).  
 void setup() {   
 pinMode (strobePin, OUTPUT);                    // zet alle pinnen als output om de shift registers aan te sturen.  
 pinMode (clockPin, OUTPUT);  
 pinMode (dataPin, OUTPUT);  
 pinMode (row_c, OUTPUT);  
 pinMode (row_b, OUTPUT);  
 pinMode (row_a, OUTPUT);  
 pinMode (resredPin, OUTPUT);  
 pinMode (lowRowPin, OUTPUT);  
 pinMode (highRowPin, OUTPUT);   
 digitalWrite (resredPin, LOW);                  // resred werkt als een schakelaar. Laag betekent display aan, hoog is display uit.  
 digitalWrite (strobePin, LOW);                  // strobe pas hoog brengen nadat de data naar het shiftregister werd gestuurd.  
 digitalWrite (lowRowPin, LOW);                  // LOW = selekteer rij 0 t/m 7.  
 digitalWrite (highRowPin, LOW);                 // LOW = selekteer rij 8 t/m 15.  
 }  
 void loop() {  
 for (int rij = 0; rij <8; rij++)                // ga alle 8 rijen af  
  {  
  setRow(rij);  
  digitalWrite(strobePin, LOW);                  // strobePin laag opdat de LEDs niet wijzigen als we de volgende bits doorsturen.   
  shiftOut(dataPin, clockPin, MSBFIRST, 255);    // stuur de databits naar het shiftregister, bit met hoogste waarde eerst (niet kritisch).  
  digitalWrite(strobePin, HIGH);                 // zet de nieuw ontvangen data op de uitgangen. Stuur bijgevolg de LEDs aan.  
  }   
 }  
 void setRow (int row)                           // deze functie wordt gebruikt voor het multiplexen van de acht rijen.  
 {  
  digitalWrite (row_a, row & 1);                 // zet decimale waarden 0 tot 7 om naar binaire waarden (000 - 111)   
  digitalWrite (row_b, row & 2);                 // om de corresponderende rowpinnen die de matrixlijnen aansturen   
  digitalWrite (row_c, row & 4);                 // te activeren.  
 }  
